/// <reference types="angular-ui-bootstrap" />
/// <reference types="angular-idle" />

import { enableProdMode, NgZone } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import {UIRouter, UrlService} from '@uirouter/core';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule).then(platformRef => {
    const urlService: UrlService = platformRef.injector.get(UIRouter).urlService;

    const startRouter = () => {
      // Instruct UIRouter to listen to URL changes
      urlService.listen();
      urlService.sync();
    };

    platformRef.injector.get(NgZone).run(startRouter);
  }
);
